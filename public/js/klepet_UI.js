/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

//slovar preimenovanih uporabnikov
var preimenovanja={};
function preimenuj(ime, vzdevek){
  preimenovanja[ime]=vzdevek;
}
function vzdevek(ime){
  if (ime in preimenovanja){
    return preimenovanja[ime]+" ("+ime+")";
  }
  else{
    return ime;
  }
}
function ime(vzd){
  for (var ime in preimenovanja){
    if (vzdevek(ime)==vzd){
      return ime;
    }
  }
  return (vzd);
}
function odstrani(vzdevek){
  var ime=ime(vzdevek);
  if (ime in preimenovanja){
    preimenovanja[ime]=null;
  }
}
function dodajVzdevke(besedilo) {
  for (var ime in preimenovanja){
    if(ime==besedilo.substring(0,ime.length)){
      besedilo=vzdevek(ime)+besedilo.substring(ime.length,besedilo.length);
      break;
    }
    if('(zasebno za '+ime+'):'==besedilo.substring(0,ime.length+14)){
      besedilo='(zasebno za '+vzdevek(ime)+'):'+besedilo.substring(ime.length+14,besedilo.length);
      break;
    }
  }
  return besedilo;
}


function dodajSlike(vhod){
  var koncniceSlik=[".jpg",".png",".gif"];
  var izraz=/\bhttp[\w\d\.,:/\-_~!*'();@&=+$?#\[\]]*\.(jpg|png|gif)\b/gi;//regularni izraz, ki poisce vse url naslove slik
  var besede={};
  var i=0;
  do {
    var beseda = izraz.exec(vhod);
    if (beseda) {
        besede[i]=beseda[0];
        i++;
    }
  } while (beseda);
  for (var i in besede){
    if(besede[i].substring(0,4)=="http" && !naslovSmeska(besede[i])){//ignorira smeske
      var dolzina=-1;
      for (var j in koncniceSlik){
        if(besede[i].indexOf(koncniceSlik[j])!=-1)
          dolzina=besede[i].indexOf(koncniceSlik[j])+4;
      }
      if(dolzina!=-1){
        $('#sporocila').append('<br/>');//dodajanje slike na stran
        $('#sporocila').append($('<img />').attr("src",besede[i].substring(0,dolzina)).attr("style","width:200px;padding-left: 20px;"));
      }
    }
  }
}

function naslovSmeska(naslov){
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var a in preslikovalnaTabela){
    if(naslov.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/"+preslikovalnaTabela[a])>-1){
      return true;
    }
  }
  return false;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 * @param spremeniImena vsebuje true/false glede na to od kod je bila funkcija klicana
 *  saj se morajo preimenovanja spremeniti samo v glavnem oknu za sporocila
 */
function divElementEnostavniTekst(sporocilo, spremeniImena) {
  if(spremeniImena)
    sporocilo=dodajVzdevke(sporocilo);
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var krcanje=sporocilo.indexOf("&#9756;")>-1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo, false);
  }else if(krcanje){
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;");
    return divElementHtmlTekst(sporocilo);
  }else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo, spremeniImena) {
   if(spremeniImena)
    sporocilo=dodajVzdevke(sporocilo);//isto kot pri divElementEnostavniTekst
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo, true));
      dodajSlike(sistemskoSporocilo);//doda slike v zasebnem sporocilu na strani posiljatelja
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo, true));
    dodajSlike(sporocilo);//doda slike sporocilu na strani posiljatelja
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo, false));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.', false));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo, true);
    if (sporocilo.spremembaVzd){
      if(sporocilo.staro in preimenovanja){
        preimenuj(sporocilo.novo,preimenovanja[sporocilo.staro]);//doda nadimek novemu vzdevku uporabnika
        delete preimenovanja[sporocilo.staro];//odstrani star zapis
      }
      else if(sporocilo.novo in preimenovanja){
        delete preimenovanja[sporocilo.novo];//če je v seznamu vzdevkov slučajo že nekdo, ki ima to ime
      }
    }
    $('#sporocila').append(novElement);
    dodajSlike(sporocilo.besedilo);//doda slike v sporocilu na strani prejemnika (tudi zasebno)
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i], false));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(vzdevek(uporabniki[i])), false);
    }
    $('#seznam-uporabnikov div').click(function() {
      var sporocilo=klepetApp.procesirajUkaz('/zasebno "' + ime($(this).text())+'" "&#9756;"');
      $('#sporocila').append(divElementHtmlTekst(sporocilo, true));
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
